### Summary

Quite often, sites need to be able to show the user the last products
they have viewed, especially for catalogs and online stores. This module
allows you to display the last user-viewed entities (filtered by certain types)
with a few clicks by means of a block. There is also integration with
the Views, which allows for more flexible customization of the output.

To configure open the page
http://example.com/admin/config/content/last_viewed
(Configuration -> Content authoring -> Last viewed)

Unlike other similar implementations, data about visited pages is stored
in cookies, rather than in sessions. This choice is due to the fact that
the use of sessions breaks the cache for anonymous users.

ATTENTION!!! Due to the fact that information about visited pages is stored
in cookies, I ask you to pay attention to the recommended use of checks
on the availability of reading entities by users.


### Requirements

None


### Installation

Install as usual, see http://drupal.org/node/895232 for further information.


### Contact

Current maintainers:
* Denis Nikiforov (WirGen) - https://www.drupal.org/user/1573728
